(function () {
    angular
        .module("weddingGramApp")
        .controller("CommentListCtrl", ["$http",'$state' ,CommentListCtrl]);

    function CommentListCtrl($http,$state) {
        var self = this;

        self.commentForm = {};
        self.comments = [];

        self.getComments = function (postId) {
            //Calling HTTP Get Comment Here
        };

        self.showComments= function (post) {
            $state.go('comments',{data : JSON.stringify(post)})

        };
        self.addComment = function (postId) {
            console.log(self.commentForm);
            //Calling HTTP Add Comment Here
        };
    }
})();
