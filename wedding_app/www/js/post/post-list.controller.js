(function () {
    angular
        .module("weddingGramApp")
        .controller("PostListCtrl", ["PostAPI", "$http", "$scope",  PostListCtrl]);

    function PostListCtrl(PostAPI, $http, $scope) {
        var self = this;
        self.modalShown = false;

        $scope.$on("updateList",function(){
       		console.log("refresh list");
                    PostAPI
                	  .me()
                      .then(function (response) {
                    	self.posts = response.data;
               	      })
                	  .catch(function (err) {
                    	console.log(err);
                	  });
    	});

        PostAPI
            .me()
            .then(function (response) {
                self.posts = response.data;
            })
            .catch(function (err) {
                console.log(err);
            });

        self.like = function (post) {
            //calling Like API
        };


        self.toggleModal = function() {
            console.log("toggle Modal !");
        };

    }
})();
