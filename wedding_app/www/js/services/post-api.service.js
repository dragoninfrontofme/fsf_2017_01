(function () {
    angular
        .module("weddingGramApp")
        .service("PostAPI", [
            "$http",
            PostAPI
        ]);

    function PostAPI($http) {
        var self = this;

        self.me = function () {
            return $http.get("http://weddinggram.getsandbox.com/api/posts/me");
        };

        self.create = function (post) {
            //do something create
        }
    }
})();